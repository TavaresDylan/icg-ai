# About ICG's AI technologies

>ℹ️ This folder concerns the global study and the study of the implementation of the ia used to generate a caption on photos. Must be used only for test purposes.

Checkout the final application => [![Github icon](https://img.icons8.com/color-glass/32/000000/github.png)](http://google.com.au/)

Made by [Dylan. Tavares](https://www.linkedin.com/in/dylan-tavar%C3%A8s-727b23187/)

## Todo list by steps

- [x] Create data download script
- [x] Create database with docker
- [ ] Insert data into database
- [ ] Data loading from database
- [ ] Text data analysis
- [ ] Picture data analysis
- [x] Text data pre-processing
    - [x] Text tokenize
    - [x] Stop words, punctuation deletion, lowercasing 
- [ ] Model training
    - [ ] VGG-16
    - [x] EfficientNet
    - [x] Xception
- [ ] Save model
    - [x] Xception
    - [ ] VGG-16
    - [ ] EfficientNet
- [ ] Merge of picture dataset and text dataset
- [ ] Testing model
    - [ ] VGG-16
    - [ ] EfficientNet
    - [x] Xception
- [ ] Output some metrics about model performance
    - [ ] VGG-16
    - [ ] EfficientNet
    - [ ] Xception
- [ ] Model tuning
    - [ ] Nb EPOCHS

## Project file structure

This AI will be firstly trained with:

- **Flicker8k_Dataset** - Dataset folder which contains **8091 images**.
- **Flickr_8k_text** - Dataset folder which contains text files and captions of images. (each image has 5 captions and we can see that #(0 to 5)number is assigned for each caption) total of **40460 lines**

The objective is to train basic model with original Flickr8k dataset and in the future insert more data into it to train model again with new data.

Using some python packages:

- **NLTK**: to clean all of our text datas
- **Pandas**: to explore easily our datas
- **Keras**: to train our AI
- **PIL**: to manage our images

### Resources

[Keras - Tutorial Image captioning](https://keras.io/examples/vision/image_captioning/)

[Dtata-flair - Totorial Image caption generator](https://data-flair.training/blogs/python-based-project-image-caption-generator-cnn/)

[Kaggle - VGG-16 and LTSM image caption generator](https://www.kaggle.com/code/shweta2407/vgg16-and-lstm-image-caption-generator/notebook)

[Kaagle - image captioning](https://www.kaggle.com/shadabhussain/automated-image-captioning-flickr8)

# Get started

