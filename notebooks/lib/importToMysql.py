import pymysql
import getpass
import os

u = input("User : ")
try:
    p = getpass.getpass()
except Exception as error:
    print('ERROR', error)
mysqlConn = pymysql.connect(host="localhost", port=3306, user=u, password=p, local_infile=True)
c = mysqlConn.cursor()
db = input("Database : ")
try:
    c.execute("CREATE DATABASE IF NOT EXISTS {0};".format(db))
    print("Database {0} created".format(db))
except Exception as error:
    print('ERROR', error)

c.execute("USE {0}".format(db))
try:
    c.execute("SET GLOBAL local_infile=1;")
    print("local_infile set to 1")
except Exception as error:
    print('ERROR', error)

c.execute("CREATE TABLE IF NOT EXISTS token (image_name varchar(255), description varchar(255))")
c.execute("CREATE TABLE IF NOT EXISTS lemma_token (image_name varchar(255), description varchar(255))")

try:
    c.execute("LOAD DATA LOCAL INFILE 'data/text/Flickr8k.token.txt' INTO TABLE token FIELDS TERMINATED BY '\t' LINES TERMINATED BY '/n';")
    print("Data token imported")
except Exception as error:
    print('ERROR', error)
    
try:
    c.execute("LOAD DATA LOCAL INFILE 'data/text/Flickr8k.lemma.token.txt' INTO TABLE lemma_token;")
    print("Data lemma.token imported")
except Exception as error:
    print('ERROR', error)
