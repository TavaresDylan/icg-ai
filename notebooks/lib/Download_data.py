from pathlib import Path
import requests
from tqdm import tqdm
from zipfile import ZipFile
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                    help='an integer for the accumulator')
parser.add_argument('--sum', dest='accumulate', action='store_const',
                    const=sum, default=max,
                    help='sum the integers (default: find the max)')

args = parser.parse_args()
print(args.accumulate(args.integers))

Path("../data").mkdir(parents=True, exist_ok=True)
print("data folder created")

pictures_url = "https://github.com/jbrownlee/Datasets/releases/download/Flickr8k/Flickr8k_Dataset.zip"
text_url = "https://github.com/jbrownlee/Datasets/releases/download/Flickr8k/Flickr8k_text.zip"

def downloadFileUrl(url, dirPath, fileName):
    response = requests.get(url, stream=True)
    total_size_in_bytes= int(response.headers.get('content-length', 0))
    block_size = 1024 #1 Kilobyte
    progress_bar = tqdm(total=total_size_in_bytes, unit='iB', unit_scale=True)
    with open(dirPath+fileName, 'wb') as file:
        for data in response.iter_content(block_size):
            progress_bar.update(len(data))
            file.write(data)
    progress_bar.close()
    if total_size_in_bytes != 0 and progress_bar.n != total_size_in_bytes:
        print("ERROR, something went wrong")
        
downloadFileUrl(text_url, "../data/", "text.zip")
downloadFileUrl(pictures_url, "../data/", "pictures.zip")

# Pictures extraction   
with ZipFile("../data/pictures.zip", 'r') as zip_ref:
    zip_ref.extractall("../data/img/")
# Text extraction
with ZipFile("../data/text.zip", 'r') as zip_ref:
    zip_ref.extractall("../data/text/")